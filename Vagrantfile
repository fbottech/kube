# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # ------
  # Set OS
  # ------
  config.vm.box = "ubuntu/xenial64"

  # --------------------------------------------
  # Enable access to Kubernetes admin panel port
  # --------------------------------------------
  config.vm.network "forwarded_port", guest: 8080, host: 8080

  # Create   
  # config.vm.synced_folder "../data", "/vagrant_data"
  
  # ---------------------
  # Allocate VM resources
  # ---------------------
  config.vm.provider "virtualbox" do |vb|
    vb.cpus = "2"
    vb.memory = "2048"
  end

  # -----------------------------
  # Update system before starting
  # -----------------------------
  config.vm.provision "system", type: "shell", inline: <<-SHELL
    apt-get -y update
    apt-get -y upgrade
  SHELL
  
  # -----------------
  # Install Docker CE
  # -----------------
  config.vm.provision "docker", type: "shell", inline: <<-SHELL
    apt-get install -y \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg-agent \
      software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
      | apt-key add -
    apt-key fingerprint 0EBFCD88
    add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"
    apt-get -y update
    apt-get install -y \
      docker-ce \
      docker-ce-cli \
      containerd.io
  SHELL

  # ----------------
  # Install Kube CTL
  # ----------------
  config.vm.provision "kubectl", type: "shell", inline: <<-SHELL
    apt-get -y update
    apt-get install -y apt-transport-https
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg \
      | apt-key add -
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" \
      | tee -a /etc/apt/sources.list.d/kubernetes.list
    apt-get -y update
    apt-get install -y kubectl
  SHELL

  # ----------------
  # Install Minikube
  # ----------------
  config.vm.provision "minikube", type: "shell", inline: <<-SHELL
    pushd /tmp
    curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    chmod +x minikube
    cp minikube /usr/local/bin && rm minikube
    popd
  SHELL

  # ----------------
  # Start Kubernetes
  # ----------------
  config.vm.provision "k8s", type: "shell", inline: <<-SHELL
    minikube start \
      --vm-driver none \
      --mount \
      --mount-string /lib/modules:/lib/modules
  SHELL

  # ---------------------------
  # Launch Kubernetes Dashboard
  # ---------------------------
  config.vm.provision "dashboard", type: "shell", inline: <<-SHELL
    kubectl apply -f \
      https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
    kubectl proxy --port=8080 &
  SHELL

  # -------------------------------------
  # Visit dashboard at the following link
  # -------------------------------------
  # http://localhost:8080/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

end
